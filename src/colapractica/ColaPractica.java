package colapractica;
import java.util.*;
/**
 *
 * @author Melina Karen Ticra Aguilar
 */
public class ColaPractica {
    public static void main(String[] args) {
        MenuCola c= new MenuCola(5);
        
        Scanner x=new Scanner(System.in);
        System.out.println("++++ Menu de colas ++++");
        
        System.out.println("1.- Añadir un nuevo elemento");
        System.out.println("2.- Borrar un elemento de la estructura cola");
        System.out.println("3.- Mostrar el numero de elementos que existe en la estructura cola");
        System.out.println("4.- Mostrar el elemento minimo y maximo");
        System.out.println("5.- Buscar un elemento dentro de la estructura cola");
        System.out.println("6.- Mostrar todos los elementos");
        System.out.println("7.- Salir");
        
        int elegir;
        int cont=0;
        while(cont==0){
            System.out.println("elija una opcion");
            elegir =x.nextInt();
            switch(elegir){
                case 1:
                    
                    System.out.println("agregue un valor a la cola");
                    int valor=x.nextInt();
                    c.push(valor);
                    break;
                    
                case 2:
                    System.out.println("se borro: " + c.pop());
                    break;
                case 3:
                    System.out.println("estan: " + c.tamaño()+ " elementos en la cola");
                    break;
                case 4:
                    break;
                case 5:
                    break;
                case 6:
                    System.out.println("Los elementos de la cola son:");
                    int tam=c.tamaño();
                    while(tam>=1){
                        System.out.println(c.pop());
                        tam--;   
                    }   
                    break;
                case 7:
                    System.out.println("saliste");
                    break;
                default:
                    System.out.println("opcion no valida");
                    break;
            }
        }
        
        
    }
    
}
