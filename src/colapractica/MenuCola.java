package colapractica;
/**
 * @author Melina Karen Ticra Aguilar 
 */
public class MenuCola {
    int [] cola;
    int tam;
    int start;
    int end;
    
    public MenuCola (int n){
        this.tam=n;
        this.start=-1;
        this.end=-1;
        this.cola= new int[tam];
        
    }
    public boolean empty(){
        return start==end;
    }
    public void push(int dato){
        if ((end+1)<=tam){
            end++;
            cola[end]=dato;
        }
    }
    public int peek(){
        if(!(start==end)){
            int aux=start;
            aux++;
            return cola[aux];
        }else{
            System.out.println("cola vacia");
            return -1;
        }  
    }
    public int pop(){
        if(!(start==end)){
            start ++;
            return cola[start];
        }else{
            System.out.println("cola vacia");
            return -1;
        }
    }
    public int tamaño(){
        return end +1;
    }
}
